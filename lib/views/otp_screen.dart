import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:otp_text_field/otp_text_field.dart';
import 'package:easy_pocket/views/home_screen.dart';

class OTPScreen extends StatelessWidget {
  OTPScreen(this._verificationId, {Key? key}) : super(key: key);

  String _verificationId = "";
  String otp = "";
  late BuildContext context;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Verify OTP"),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(top: 70.0),
            child: const Text(
              "Please enter your OTP",
              style: TextStyle(fontSize: 25.0),
            ),
          ),
          const SizedBox(
            height: 30.0,
          ),
          Container(
            padding: const EdgeInsets.only(top: 20.0),
            child: OTPTextField(
              length: 6,
              width: MediaQuery.of(context).size.width,
              textFieldAlignment: MainAxisAlignment.spaceAround,
              fieldWidth: 50.0,
              keyboardType: TextInputType.number,
              outlineBorderRadius: 15,
              style: const TextStyle(fontSize: 20),
              onCompleted: (pin) {
                log(pin);
                otp = pin;
              },
            ),
          ),
          const SizedBox(
            height: 50.0,
          ),
          Container(
            margin: const EdgeInsets.only(left: 10.0, right: 10.0),
            width: double.infinity,
            child: MaterialButton(
              color: Colors.blue,
              onPressed: () {
                log("Entered Otp is $otp");
                signInWithPhoneNumber(context);
              },
              child: const Text(
                "Submit",
                style: TextStyle(color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }

  void signInWithPhoneNumber(BuildContext context) async {
    try {
      AuthCredential credential = PhoneAuthProvider.credential(
          verificationId: _verificationId, smsCode: otp);
      await _auth.signInWithCredential(credential);
      log("Successfully Logged In");
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text("Successfully Logged In"),
        ),
      );
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const HomeScreen(),
        ),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("Error while signing in $e"),
        ),
      );
    }
  }
}
