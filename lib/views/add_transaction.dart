import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AddTransaction extends StatefulWidget {
  const AddTransaction({Key? key}) : super(key: key);

  @override
  _AddTransactionState createState() => _AddTransactionState();
}

class _AddTransactionState extends State<AddTransaction> {
  DateTime _selectedDate = DateTime.now();
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final TextEditingController _amount = TextEditingController();
  final TextEditingController _note = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Add Transaction"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                right: 10.0,
                left: 10.0,
                top: 20.0,
              ),
              child: TextFormField(
                controller: _amount,
                // initialValue: "\$",
                style: const TextStyle(fontSize: 25.0),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  labelText: "Total Amount in (Rs)",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                right: 10.0,
                left: 10.0,
                top: 20.0,
              ),
              child: TextFormField(
                onTap: () {
                  selectDate();
                },
                maxLines: null,
                style: const TextStyle(fontSize: 25.0),
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  hintText:
                      "Date : ${_selectedDate.day}-${_selectedDate.month}-${_selectedDate.year}",
                  hintStyle: const TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                right: 10.0,
                left: 10.0,
                top: 20.0,
              ),
              child: TextFormField(
                controller: _note,
                maxLines: null,
                keyboardType: TextInputType.multiline,
                style: const TextStyle(fontSize: 25.0),
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  labelText: "Note: ",
                ),
              ),
            ),
            const SizedBox(
              height: 100.0,
            ),
            Container(
              height: 50.0,
              padding: const EdgeInsets.only(
                left: 10.0,
                right: 10.0,
              ),
              width: double.maxFinite,
              child: MaterialButton(
                elevation: 3,
                color: Colors.blue,
                child: const Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  inputData(_amount.text, _note.text);
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Successfully Submitted"),
                      duration: Duration(seconds: 4),
                    ),
                  );
                  Navigator.pop(context);
                },
              ),
            ),
            const SizedBox(
              height: 17.0,
            ),
            Container(
              margin:
                  const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 20.0),
              height: 50.0,
              width: double.maxFinite,
              decoration: BoxDecoration(border: Border.all(color: Colors.blue)),
              child: MaterialButton(
                elevation: 3,
                child: const Text("Cancel"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            )
          ],
        ),
      ),
      resizeToAvoidBottomInset: false,
    );
  }

  selectDate() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2100),
    ) as DateTime;
    setState(() {
      _selectedDate = picked;
    });
    return _selectedDate;
  }

  void inputData(String amount, String note) {
    Map<String, dynamic> data = {};
    data.addAll({
      "Amount": amount,
      "Date":
          "${_selectedDate.day}-${_selectedDate.month}-${_selectedDate.year}",
      "Note": note
    });

    User user = _auth.currentUser!;
    var uid = user.uid;
    CollectionReference refrence = FirebaseFirestore.instance.collection(uid);
    refrence.add(data);
  }
}
