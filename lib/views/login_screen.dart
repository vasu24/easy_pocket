import 'dart:developer';
import 'package:easy_pocket/views/otp_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _mobileNumber = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String _verificationId = "";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(25.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 110.0,
                ),
                const Center(
                  child: FlutterLogo(
                    size: 120.0,
                  ),
                ),
                const SizedBox(
                  height: 50.0,
                ),
                const Text(
                  "LOGIN",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 25.0,
                  ),
                ),
                const SizedBox(
                  height: 40.0,
                ),
                Container(
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black)),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: TextField(
                      controller: _mobileNumber,
                      keyboardType: TextInputType.number,
                      style: const TextStyle(
                        fontSize: 25.0,
                        color: Colors.black,
                      ),
                      decoration: const InputDecoration(
                        hintText: "MOBILE NUMBER",
                        hintStyle: TextStyle(
                          fontSize: 25.0,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20.0,
                ),
                SizedBox(
                  height: 50.0,
                  width: double.maxFinite,
                  child: MaterialButton(
                    onPressed: () {
                      log(_mobileNumber.text);
                      verifyPhoneNumber();
                    },
                    color: Colors.black,
                    child: const Text(
                      "LOGIN",
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 40.0,
                ),
                const Center(
                  child: Text(
                    "By Logging In, you agree our terms and condition",
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  verifyPhoneNumber() {
    _auth.verifyPhoneNumber(
        timeout: const Duration(seconds: 60),
        phoneNumber: "+91${_mobileNumber.text}",
        verificationCompleted: (PhoneAuthCredential credential) async {
          await _auth.signInWithCredential(credential);
        },
        verificationFailed: (FirebaseAuthException e) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text("The entered phone number is incorrect"),
            ),
          );
        },
        codeSent: (String verificationId, int? resendToken) {
          _verificationId = verificationId;
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OTPScreen(_verificationId),
            ),
          );
          log("code is sent successfully");
        },
        codeAutoRetrievalTimeout: (String verificationId) {
          log("code time out");
        });
  }
}
