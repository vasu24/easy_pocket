import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:easy_pocket/views/add_transaction.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  late CollectionReference users;
  late Map<String, dynamic> data = {};
  var snapshotData;
  var finalData;

  @override
  void initState() {
    super.initState();
  }

  getUserData() async {
    log("into the getUserData");
    User user = _auth.currentUser!;
    log("user : $user");
    log("after user");
    var uid = user.uid;
    log("uid : $uid");
    log("after uid");
    var users = await FirebaseFirestore.instance.collection(uid)
        .snapshots()
        .listen((event) {
          finalData = event.docs;
      log("value of event : ${event.docs}");
    });
    // CollectionReference users = FirebaseFirestore.instance.collection(uid).snapshots() as CollectionReference<Object?>;

    Map<String, dynamic> mapData = finalData as Map<String, dynamic>;
    log("mapData is : ${mapData["Amount"]}");
    data = mapData;
    log("Data is : ${data["Amount"]}");
    return data;
  }

  @override
  Widget build(BuildContext context) {
    getUserData();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home Screen"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  height: 50.0,
                  width: 200.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                  ),
                  child: const Center(
                    child: Text(
                      "Total Expense",
                      style: TextStyle(fontSize: 25.0),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10.0,
                  child: Text(
                    "=",
                    style: TextStyle(fontSize: 30.0),
                  ),
                ),
                Container(
                  height: 50.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                  ),
                  child: const Center(
                    child: Text(
                      "0\$",
                      style: TextStyle(fontSize: 25.0),
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  height: 50.0,
                  width: 200.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                  ),
                  child: const Center(
                    child: Text(
                      "Total Income",
                      style: TextStyle(fontSize: 25.0),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10.0,
                  child: Text(
                    "=",
                    style: TextStyle(fontSize: 30.0),
                  ),
                ),
                Container(
                  height: 50.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                  ),
                  child: const Center(
                    child: Text(
                      "1000\$",
                      style: TextStyle(fontSize: 25.0),
                    ),
                  ),
                )
              ],
            ),
          ),
          const SizedBox(
            height: 50.0,
          ),
          SingleChildScrollView(
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: data.length,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Text("Amount : ${finalData[index]}"),
                    Text("Date : ${data[index]["Date"]}"),
                    Text("Note : ${data[index]["Note"]}"),
                  ],
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const AddTransaction(),
            ),
          );
        },
      ),
    );
  }
}
